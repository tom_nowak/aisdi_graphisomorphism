#include "Graph.h"
#include <stdexcept>
#include <algorithm>


#define DEBUG(x) // x for debugging, empty macro otherwise


// ****************************************
// Graph interface functions:
// ****************************************

aisdi::Graph::Graph(std::istream &input)
{
    size_t size;
    input >> size;
    if(!input)
        throw std::runtime_error("Input error in Graph constructor - unable to read number of vertices.");
    allocateVectors(size);
    for(;;)
    {
        size_t v1, v2;
        input >> v1;
        if(!input)
            break;
        input >> v2;
        if(!input)
            throw std::runtime_error("Input error in Graph constructor - missing second vertex number from pair.");
        addEdge(v1, v2);
    }
    std::sort(vertices.begin(), vertices.end());
}

void aisdi::Graph::addEdge(size_t v1, size_t v2)
{
    if(v1 >= vertices.size() || v2 >= vertices.size()) // >=, not = because of 0-based indexing
        throw std::runtime_error("Cannot add edge - vertex number exceeds graph size.");
    size_t index = adjacencyMatrix.getIndex(v1, v2);
    // It is impossible to add the same edge degree twice, because it is checked if a given
    // pair of vertices has already been connected (and warning is printed - look below).
    if(adjacencyMatrix.data[index] != 0)
    {
        std::cerr << "Warning - ignored request to add second edge between vertices: "
                  << v1 << ", " << v2 << "\n";
        return;
    }
    adjacencyMatrix.data[index] = 1;
    vertices[v1].increaseDegree();
    // Even in case of v1==v2 first increase degree, then change sign of degree -
    // otherwise "connecting" vertex of degree 0 with itself would have no result.
    if(v1==v2)
        vertices[v1].degree = -vertices[v1].degree; // my convention for marking vertex connected with itself
    else
        vertices[v2].increaseDegree();
}

bool aisdi::Graph::checkIsomorphism(aisdi::Graph &graph, std::ostream &output)
{
    Transformation transformation;
    switch(transformation.setUp(vertices, graph.vertices))
    {
        case Transformation::sizes_do_not_match:
            output << "Non - isomorphic (sizes do not match)\n";
            return 0;
        case Transformation::degrees_do_not_match:
            output << "Non - isomorphic (degrees do not match)\n";
            return 0;
        default: // Transformation::ok
            do
            {
                DEBUG(std::cout << "Checking transform:\n"; transformation.print(std::cout); std::cout << "\n";)
                if(adjacencyMatrix.compare(graph.adjacencyMatrix, transformation.indicesTransformation))
                {
                    output << "Isomorphic\n";
                    transformation.print(output);
                    return 1;
                }
            } while(transformation.nextPermuatation());
            output << "Non - isomorphic (checked all permuatations)\n";
            return 0;
    }
}


// ****************************************
// Pseudo-private Graph functions:
// ****************************************

void aisdi::Graph::allocateVectors(const size_t size)
{
    vertices.reserve(size);
    for(size_t i=0; i<size; ++i)
        vertices.emplace_back(i);
    adjacencyMatrix.allocate(size);
    DEBUG(std::cout << "allocateVectors: vertices(" << vertices.size()
                    << ") adjacencyMatrix (" << adjacencyMatrix.data.size() << ")\n";)
}


// ****************************************
// Functions in auxiliary data structures:
// ****************************************

// ****************************************
// Functions in aisdi::Graph::Vertex:
// ****************************************

void aisdi::Graph::Vertex::increaseDegree()
{
    if(degree<0)
        --degree;
    else
        ++degree;
}

bool aisdi::Graph::Vertex::operator<(const aisdi::Graph::Vertex &v) const
{
    if(degree!=v.degree)
        return degree<v.degree;
    return number<v.number;
}


// ****************************************
// Functions in aisdi::Graph::AdjacencyMatrix:
// ****************************************

void aisdi::Graph::AdjacencyMatrix::allocate(const size_t size)
{
    const size_t matSize = (size*(size+1))/2; // 1+2+...+size
    data.resize(matSize);
    for(size_t i=0; i<matSize; ++i)
        data[i] = 0;
}

size_t aisdi::Graph::AdjacencyMatrix::getIndex(size_t vertex1, size_t vertex2) const
{
    size_t x, y;
    if(vertex1<vertex2)
    {
        x = vertex1;
        y = vertex2;
    }
    else
    {
        x = vertex2;
        y = vertex1;
    }
    /* already well tested - disable DEBUG even if it is active elsewhere
    DEBUG(std::cout << "AdjacencyMatrix::getIndex(" << vertex1 << ", "
                    << vertex2 << "): " << ((y+1)*y)/2 + x << "\n"; )
    */
    // ((y+1)*y)/2 - total number of elements in rows with indices less than y, x - offset in row number y
    return ((y+1)*y)/2 + x;
}

bool aisdi::Graph::AdjacencyMatrix::compare(const aisdi::Graph::AdjacencyMatrix &mat,
                                            const std::vector<size_t> &transformation) const
{
    // *this and mat should be created by function AdjacencyMatrix::allocate
    const size_t size = transformation.size();
    for(size_t i=0; i<size; ++i) // for each row
    {
        for(size_t j=0; j<=i; ++j) // for each column (not including diagonal)
        {
            if((*this)(j, i) != mat(transformation[j], transformation[i]))
                return 0;
        }
    }
    return 1;
}


// ****************************************
// STL template 'hack' for aisdi::Graph::VertexTransformation:
// ****************************************

namespace std
{
    template<>
    inline void swap(aisdi::Graph::VertexTransformation &td1,
                     aisdi::Graph::VertexTransformation &td2) noexcept
    {
        // swap values pointed by pointers, not the pointes itself
        swap(*td1.mappedIndex, *td2.mappedIndex);
    }
}


// ****************************************
// Functions in aisdi::Graph::TransformationMap:
// ****************************************

aisdi::Graph::Transformation::State aisdi::Graph::Transformation::setUp(
        const std::vector<aisdi::Graph::Vertex> &v1, const std::vector<aisdi::Graph::Vertex> &v2)
{
#define DEBUG2(x) // this function was difficult to write - DEBUG can be enabled even if global DEBUG is disabled
    const size_t size = v1.size();
    if(size != v2.size())
        return sizes_do_not_match;
    if(size==0) // check must be done here, because I use v1[0].degree before the loop
       return ok;
    indicesTransformation.resize(size);
    verticesPermuatation.reserve(size);
    iterators.push_back(verticesPermuatation.begin());
    long int deg, lastDegree = v1[0].degree;
    DEBUG2( std::cout << "Transformation::setUp:\n"; )
    for(size_t i=0; i<size; ++i)
    {
        deg = v1[i].degree;
        if(deg != v2[i].degree)
            return degrees_do_not_match;
        size_t addedIndex = v1[i].number;
        indicesTransformation[addedIndex] = v2[i].number; // initial transformation - not changed order of vertices
        DEBUG2( std::cout << "indices[" << addedIndex << "] = " << v2[i].number << " (degree: " << deg << ")\n"; )
        if(deg==0 || deg==-1)
            continue;
        verticesPermuatation.emplace_back(indicesTransformation.data() + addedIndex); // pointer to last element saved in indices
        DEBUG2( std::cout << "added address of " << *verticesPermuatation.back().mappedIndex << " to vertices\n"; )
        if(deg != lastDegree)
        {
            lastDegree = deg;
            DEBUG2( std::cout << "new iterator saved\n"; )
            iterators.push_back(verticesPermuatation.end()-1);
        }
    }
    iterators.push_back(verticesPermuatation.end());
    DEBUG2(
            std::cout << "Transformation vertices are divided into:\n";
            auto itBegin = iterators.begin();
            auto itEnd = itBegin;
            while(++itEnd != iterators.end())
            {
                std::for_each(*itBegin, *itEnd, [](VertexTransformation &v) { std::cout<<" "<<*v.mappedIndex; });
                std::cout << "\n";
                itBegin = itEnd;
            }
            std::cout << "\n";
         )
    return ok;
#undef DEBUG2
}

bool aisdi::Graph::Transformation::nextPermuatation()
{
    auto itBegin = iterators.begin();
    DEBUG(
            std::cout << "nextPermutation:\n";
            if(itBegin == iterators.end())
            {
                std::cout << "\tnextPermutation error - empty vector\n";
                return 0;
            }
         )
    auto itEnd = itBegin;
    while(++itEnd != iterators.end())
    {

        DEBUG(
                std::cout << " checking:";
                std::for_each(*itBegin, *itEnd, [](VertexTransformation &v) { std::cout<<" "<<*v.mappedIndex; });
                std::cout << "\n";
             )
        if(std::next_permutation(*itBegin, *itEnd))
        {
            DEBUG(
                    std::cout << " replaced last one with:";
                    std::for_each(*itBegin, *itEnd, [](VertexTransformation &v) { std::cout<<" "<<*v.mappedIndex; });
                    std::cout << "\nnextPermutation success\n";
                 )
            return 1;
        }
        DEBUG(
                std::cout << " reversed last one:";
                std::for_each(*itBegin, *itEnd, [](VertexTransformation &v) { std::cout<<" "<<*v.mappedIndex; });
                std::cout << "\n";
             )
        itBegin = itEnd;
    }
    DEBUG( std::cout << "nextPermutation end\n"; )
    return 0;
}

void aisdi::Graph::Transformation::print(std::ostream &output)
{
    for(size_t i=0; i<indicesTransformation.size(); ++i)
        output << i << " --> " << indicesTransformation[i] << "\n";
}
