#include "Graph.h"
#include "Test.h"
#include <fstream>
#include <cstdlib>

static int standardRun(const char *filename1, const char *filename2)
{
    std::ifstream file(filename1);
    if(!file.is_open())
    {
        std::cerr << "Could not open file " << filename1 << "\n";
        return 2;
    }
    aisdi::Graph g1(file);
    file.close();
    file.open(filename2);
    if(!file.is_open())
    {
        std::cerr << "Could not open file " << filename2 << "\n";
        return 2;
    }
    aisdi::Graph g2(file);
    file.close();
    g1.checkIsomorphism(g2, std::cout);
    return 0;
}

int main(int argc, char **argv)
{
    switch(argc)
    {
        case 2:
            std::cout << "Program with 1 argument - running random test...\n";
            aisdi::graph_test::performTest(std::atoi(argv[1]));
            return 0;
        case 3:
            return standardRun(argv[1], argv[2]);
        case 4:
            std::cout << "Program with 3 arguments - running random test with graph saving...\n";
            aisdi::graph_test::performTest(std::atoi(argv[1]), argv[2], argv[3]);
            return 0;
        default:
            std::cout << "Usage: " << argv[0] << " with:\n"
                         " - 1 argument: <number of vertices in random test>\n"
                         " - 2 arguments: <path to file with graph 1> <path to file with graph 2>\n"
                         " - 3 arguments: <number of vertices in random test> "
                         "<file to save graph1> <file to save graph2>\n";
            return 1;
    }
}
