#pragma once

namespace aisdi
{
    namespace graph_test
    {
        void performTest(unsigned number, const char *file1 = 0, const char *file2 = 0);
    }
}
