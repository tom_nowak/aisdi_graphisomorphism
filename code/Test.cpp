#include "Test.h"
#include "Graph.h"
#include <cstdlib>
#include <ctime>
#include <algorithm>
#include <sstream>
#include <fstream>
#include <string>

#define DEBUG(x) // x for debugging, empty macro otherwise

static void saveGraph(std::stringstream &str, const char *filename)
{
    std::ofstream file(filename);
    if(!file.is_open())
        return;
    file << str.str();
    file.close();
}


void aisdi::graph_test::performTest(unsigned number, const char *file1, const char *file2)
{
    std::cout << "performTest - creating graphs...\n";
    std::srand(std::time(0));
    std::vector<unsigned> permutation(number);
    for(unsigned i=0; i<number; ++i)
        permutation[i] = i;
    std::random_shuffle(permutation.begin(), permutation.end());
    DEBUG(
            for(unsigned i=0; i<number; ++i)
                std::cout << "permuatation[" << i << "] = " << permutation[i] << "\n";
         )
    aisdi::Graph::AdjacencyMatrix mat1, mat2;
    mat1.allocate(number);
    mat2.allocate(number);
    for(unsigned i=0; i<number; ++i)
    {
        for(unsigned j=0; j<=i; ++j)
        {
            bool tmp = rand()%2;
            size_t index1 = mat1.getIndex(j,i);
            size_t index2 = mat2.getIndex(permutation[j],permutation[i]);
            DEBUG(
                    std::cout << "performTest - set " << tmp << " at mat1(" << j << "," << i << ") "
                                 "and at mat2(" << permutation[j] << "," << permutation[i] << ")\n";
                 )
            mat1.data[index1] = mat2.data[index2] = tmp;
        }
    }
    std::stringstream string1, string2;
    string1 << number << "\n";
    string2 << number << "\n";
    for(unsigned i=0; i<number; ++i)
    {
        for(unsigned j=0; j<=i; ++j)
        {
            if(mat1(j,i))
                string1 << i << " " << j << "\n";
            if(mat2(j,i))
                string2 << i << " " << j << "\n";
        }
    }
    aisdi::Graph g1(string1);
    aisdi::Graph g2(string2);
    if(file1 && file2)
    {
        saveGraph(string1, file1);
        saveGraph(string2, file2);
    }
    std::cout << "\nTest graphs which should be isomorphic:\n\n"
                 "First:\n" << string1.str() << "\nSecond:\n" << string2.str() << "\n"
                 "checkIsomorphism output:\n";
    if(g1.checkIsomorphism(g2, std::cout))
        std::cout << "\nTest passed.\n";
    else
        std::cout << "\nTest failed!\n";
}
