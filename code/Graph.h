#pragma once
#include <vector>
#include <iostream>

// General ideas:
// 1. Assumptions.
// My algoritm is based on the following assumptons:
// * Finding graph isomorphism is a very computation-intensive task, for big graphs it
// is practically impossible, so graphs must be small (not including special cases like
// not - matching graph sizes).
// * Time complexity is the only problem, space complexity is not.
// * If storing 'redundant' data can speed up the algorithm in any way, I do it.
// * Graphs are not extremely sparse (I use adjacency matrices - look 2., not lists).
// * Data in input files can be incorrect - in this case exception is thrown.
//
// 2. Way of storing graph.
// Graphs are stored as lower triangular bool adjacency matrices, but there is also
// 'redundant' data - vertices sorted according to their degrees and vector of iterators
// dividing vertices vector into parts of the same degree. Moreover, there are temporary
// data structures used only in function checking graph isomorphism.
//
// 3. General algorith concept - this is 'semi-brute-force' algorithm.
// First, simple checks are performed - graphs must have the same numbers of vertices and
// the same numbers of vertices of each degree (according to 'redundant' vertices divison).
// Then, I check if graphs are the same. If no, I check permutations of vertices of
// the same degree until one of them makes graphs the same - this is isomorphism.
// If none of the permutations makes isomorphism - graphs are non-isomorphic.
// Permuatations of vertices without edges or connected only with itself
// do not have impact on isomorhpism, so they are not checked
// (this can reduce number of pesimistic cases like sparse graphs).
//
// 4. Complexity.
// Pesimistic complexity is the same as in full brute-force algorithm (checking ALL permutations):
// O(N! * N^2) - N! for all permutations, N^2 for comparing adjacency matrices, N - number of vertices.
// However, in almost all cases ths algorithm will perform much better, because many graphs can
// be discarded as non-isomorphic only with the inital check and even if these checks are passed,
// there should be much less permutations to test
// (unless all vertices have the same degree - this is pesimistic case).

namespace aisdi
{
    struct Graph
    {
    // interface functions:
        Graph(std::istream &input);
        void addEdge(size_t v1, size_t v2);
        bool checkIsomorphism(Graph &graph, std::ostream &output);

    // functions that could be private, but I don't care about encapsulation:
        void allocateVectors(const size_t size);

    // auxiliary data structures:
        struct Vertex
        {
            size_t number;
            long int degree; // signed type for "special" convention - degree<0 for vertices connected with themselves
            Vertex(size_t num) : number(num), degree(0) {}
            void increaseDegree();
            bool operator<(const Vertex &v) const;
        };

        struct AdjacencyMatrix // lower triangular matrix (graph is not directed)
        {
            std::vector<bool> data;

            void allocate(const size_t size);
            size_t getIndex(size_t vertex1, size_t vertex2) const;
            bool operator()(size_t vertex1, size_t vertex2) const
            {
                return data[getIndex(vertex1, vertex2)];
            }
            // check if *this matrix and 'mat' transformed with 'transformation' are the same:
            bool compare(const AdjacencyMatrix &mat, const std::vector<size_t> &transformation) const;
        };

        struct VertexTransformation
        {
            size_t *mappedIndex;
            VertexTransformation(size_t *index) : mappedIndex(index) {}
            bool operator<(const VertexTransformation &td) { return *mappedIndex < *td.mappedIndex; }
        };

        struct Transformation
        {
            enum State { sizes_do_not_match, degrees_do_not_match, ok };

            // indicesTransformation[i] = j <=> transforming i --> j
            std::vector<size_t> indicesTransformation;

            // iterators dividing verticesPermuatation into subvectors (look below)
            std::vector<std::vector<VertexTransformation>::iterator> iterators;

            // verticesPermuatation contains pointers to appropriate elements of indicesTransformation.
            // These pointers are stored in the following order:
            // - pointers to numbers of vertices of the same degree are always next to one another
            // - each subvector containing values mentioned line above has begin and end saved in iterators
            // - initially, in each subvector, numbers of vertices pointed by are sorted ascending
            // - then, values pointed by each verticesPermuatation subvector can be permuatated
            std::vector<VertexTransformation> verticesPermuatation;

            Transformation() {}

            // initializes data structures inside Transformation and returns enum defined above
            State setUp(const std::vector<Vertex> &v1, const std::vector<Vertex> &v2); // v1, v2 must be sorted

            // Function nextPermuatation calls std::next_permutation for appropriate subvectors of
            // verticesPermuatation (according to iterators) or returns false if actual permutation is the last one.
            // Note that graphs comparison will be based only on vector of indicesTransformation and adjacency matrices
            // in graphs - verticesPermuatation and iterators are meant only to transform indicesTransformation.
            bool nextPermuatation();

            void print(std::ostream &output);
        };

    // data stored in Graph:
        // vertices vector may seem unnecesary, as there is adjacency matrix, but it will be useful for
        // simpler initialization of Trasformation data structure in function checkIsomorphism
        std::vector<Vertex> vertices;
        AdjacencyMatrix adjacencyMatrix;
    };
}
